grammar Asp;

options {
  language = Java;
}

@parser::header {
import de.egore911.aspparser.callstack.*;
import de.egore911.aspparser.exceptions.*;
import java.util.HashSet;
import java.util.Set;
}

@parser::members {

	private final Set<String> implicitVariables = new HashSet<>();

	public Set<String> getImplicitVariables() {
		return implicitVariables;
	}

	private CallStackElement root = new FileElement("", null, null, "");
	private CallStackElement current = root;

	public void init() throws RecognitionException {

		// http://www.w3schools.com/asp/asp_ref_session.asp
		CallStackElement session = new FileElement("session", ElementType.VARIABLE, root, "globally available");
		root.addChild(session, this, _input, _ctx);
		// Collections
		session.addChild(new VariableElement("contents", ElementType.VARIABLE, session, "globally available"), this, _input, _ctx);
		session.addChild(new VariableElement("staticObjects", ElementType.VARIABLE, session, "globally available"), this, _input, _ctx);
		// Properties
		session.addChild(new VariableElement("codepage", ElementType.VARIABLE, session, "globally available"), this, _input, _ctx);
		session.addChild(new VariableElement("lcid", ElementType.VARIABLE, session, "globally available"), this, _input, _ctx);
		session.addChild(new VariableElement("sessionid", ElementType.VARIABLE, session, "globally available"), this, _input, _ctx);
		session.addChild(new VariableElement("timeout", ElementType.VARIABLE, session, "globally available"), this, _input, _ctx);
		// Methods
		session.addChild(new FunctionElement("abandon", ElementType.FUNCTION, session, "globally available"), this, _input, _ctx);
		session.addChild(new FunctionElement("contents.remove", ElementType.FUNCTION, session, "globally available"), this, _input, _ctx);

		// http://www.w3schools.com/asp/asp_ref_response.asp
		CallStackElement response = new FileElement("response", ElementType.VARIABLE, root, "globally available");
		root.addChild(response, this, _input, _ctx);
		// Collections
		response.addChild(new VariableElement("cookies", ElementType.VARIABLE, session, "globally available"), this, _input, _ctx);
		// Properties
		response.addChild(new VariableElement("buffer", ElementType.VARIABLE, response, "globally available"), this, _input, _ctx);
		response.addChild(new VariableElement("cachecontrol", ElementType.VARIABLE, response, "globally available"), this, _input, _ctx);
		response.addChild(new VariableElement("charset", ElementType.VARIABLE, response, "globally available"), this, _input, _ctx);
		response.addChild(new VariableElement("contenttype", ElementType.VARIABLE, response, "globally available"), this, _input, _ctx);
		response.addChild(new VariableElement("expires", ElementType.VARIABLE, response, "globally available"), this, _input, _ctx);
		response.addChild(new VariableElement("expiresabsolute", ElementType.VARIABLE, response, "globally available"), this, _input, _ctx);
		response.addChild(new VariableElement("isclientconnected", ElementType.VARIABLE, response, "globally available"), this, _input, _ctx);
		response.addChild(new VariableElement("pics", ElementType.VARIABLE, response, "globally available"), this, _input, _ctx);
		response.addChild(new VariableElement("status", ElementType.VARIABLE, response, "globally available"), this, _input, _ctx);
		// Methods
		response.addChild(new FunctionElement("addheader", ElementType.FUNCTION, response, "globally available"), this, _input, _ctx);
		response.addChild(new FunctionElement("appendtolog", ElementType.FUNCTION, response, "globally available"), this, _input, _ctx);
		response.addChild(new FunctionElement("binarywrite", ElementType.FUNCTION, response, "globally available"), this, _input, _ctx);
		response.addChild(new FunctionElement("clear", ElementType.FUNCTION, response, "globally available"), this, _input, _ctx);
		response.addChild(new FunctionElement("end", ElementType.FUNCTION, response, "globally available"), this, _input, _ctx);
		response.addChild(new FunctionElement("flush", ElementType.FUNCTION, response, "globally available"), this, _input, _ctx);
		response.addChild(new FunctionElement("redirect", ElementType.FUNCTION, response, "globally available"), this, _input, _ctx);
		response.addChild(new FunctionElement("write", ElementType.FUNCTION, response, "globally available"), this, _input, _ctx);

		// http://www.w3schools.com/asp/asp_ref_server.asp
		CallStackElement server = new FileElement("server", ElementType.VARIABLE, root, "globally available");
		root.addChild(server, this, _input, _ctx);
		// Properties
		server.addChild(new VariableElement("scripttimeout", ElementType.VARIABLE, server, "globally available"), this, _input, _ctx);
		// Methods
		server.addChild(new FunctionElement("createobject", ElementType.FUNCTION, server, "globally available"), this, _input, _ctx);
		server.addChild(new FunctionElement("execute", ElementType.FUNCTION, server, "globally available"), this, _input, _ctx);
		server.addChild(new FunctionElement("getlasterror", ElementType.FUNCTION, server, "globally available"), this, _input, _ctx);
		server.addChild(new FunctionElement("htmlencode", ElementType.FUNCTION, server, "globally available"), this, _input, _ctx);
		server.addChild(new FunctionElement("mappath", ElementType.FUNCTION, server, "globally available"), this, _input, _ctx);
		server.addChild(new FunctionElement("transfer", ElementType.FUNCTION, server, "globally available"), this, _input, _ctx);
		server.addChild(new FunctionElement("urlencode", ElementType.FUNCTION, server, "globally available"), this, _input, _ctx);

		// http://www.w3schools.com/asp/asp_ref_request.asp
		CallStackElement request = new FileElement("request", ElementType.VARIABLE, root, "globally available");
		root.addChild(request, this, _input, _ctx);
		// Collections
		request.addChild(new VariableElement("clientcertificate", ElementType.VARIABLE, request, "globally available"), this, _input, _ctx);
		request.addChild(new VariableElement("cookies", ElementType.VARIABLE, request, "globally available"), this, _input, _ctx);
		request.addChild(new VariableElement("form", ElementType.VARIABLE, request, "globally available"), this, _input, _ctx);
		request.addChild(new VariableElement("querystring", ElementType.VARIABLE, request, "globally available"), this, _input, _ctx);
		request.addChild(new VariableElement("servervariables", ElementType.VARIABLE, request, "globally available"), this, _input, _ctx);
		// Properties
		request.addChild(new VariableElement("totalbytes", ElementType.VARIABLE, request, "globally available"), this, _input, _ctx);
		// Methods
		request.addChild(new FunctionElement("binaryread", ElementType.FUNCTION, request, "globally available"), this, _input, _ctx);

		// http://www.w3schools.com/asp/asp_ref_error.asp
		CallStackElement asperror = new ClassElement("asperror", root, "globally available");
		root.addChild(asperror, this, _input, _ctx);
		// Properties
		asperror.addChild(new VariableElement("aspcode", ElementType.VARIABLE, request, "globally available"), this, _input, _ctx);
		asperror.addChild(new VariableElement("aspdescription", ElementType.VARIABLE, request, "globally available"), this, _input, _ctx);
		asperror.addChild(new VariableElement("category", ElementType.VARIABLE, request, "globally available"), this, _input, _ctx);
		asperror.addChild(new VariableElement("column", ElementType.VARIABLE, request, "globally available"), this, _input, _ctx);
		asperror.addChild(new VariableElement("description", ElementType.VARIABLE, request, "globally available"), this, _input, _ctx);
		asperror.addChild(new VariableElement("file", ElementType.VARIABLE, request, "globally available"), this, _input, _ctx);
		asperror.addChild(new VariableElement("line", ElementType.VARIABLE, request, "globally available"), this, _input, _ctx);
		asperror.addChild(new VariableElement("number", ElementType.VARIABLE, request, "globally available"), this, _input, _ctx);
		asperror.addChild(new VariableElement("source", ElementType.VARIABLE, request, "globally available"), this, _input, _ctx);
		// Methods
		CallStackElement raise = new FunctionElement("raise", ElementType.FUNCTION, request, "globally available");
		asperror.addChild(raise, this, _input, _ctx);
		raise.addChild(new VariableElement("number", ElementType.PARAMETER, request, "globally available"), this, _input, _ctx);
		raise.addChild(new VariableElement("source", ElementType.PARAMETER, request, "globally available"), this, _input, _ctx);
		raise.addChild(new VariableElement("description", ElementType.PARAMETER, request, "globally available"), this, _input, _ctx);
		// Instance
		CallStackElement err = new VariableElement("err", ElementType.VARIABLE, root, "globally available");
		((VariableElement) err).setPrototype(asperror);
		root.addChild(err, this, _input, _ctx);

		// Core ASP
		root.addChild(new FunctionElement("randomize", ElementType.FUNCTION, root, "globally available"), this, _input, _ctx);
		root.addChild(new FunctionElement("option", ElementType.SUB, root, "globally available"), this, _input, _ctx);
		root.addChild(new FunctionElement("vartype", ElementType.FUNCTION, root, "globally available"), this, _input, _ctx);
		root.addChild(new FunctionElement("typename", ElementType.FUNCTION, root, "globally available"), this, _input, _ctx);
		// RegExp
		ClassElement regexp = new ClassElement("regexp", root, "globally available");
		root.addChild(regexp, this, _input, _ctx);
		regexp.addChild(new VariableElement("pattern", ElementType.VARIABLE, regexp, "globally available"), this, _input, _ctx);
		regexp.addChild(new VariableElement("global", ElementType.VARIABLE, regexp, "globally available"), this, _input, _ctx);
		regexp.addChild(new VariableElement("ignorecase", ElementType.VARIABLE, regexp, "globally available"), this, _input, _ctx);

	}

	private void checkDuplicate(CallStackElement element) throws RecognitionException {
		CallStackElement existing = current.getChild(element.name.toLowerCase());
		if (existing != null) {
			_errHandler.reportError(this, new DeclarationException(element.type + " " + element.name + " already defined as " + existing.type + " in " + existing.occurrence, this, _input, _ctx));
		} else {
			current.addChild(element, this, _input, _ctx);
		}
	}

	private CallStackElement isValidCallStack(CallStackElement ptrDown, List<Token> variablePart) throws LookupException {
		boolean partialFound = false;
		for (Object variablePartToken : variablePart) {
			String vp = ((Token) variablePartToken).getText();
			CallStackElement child = ptrDown.isAllowedChild(vp.toLowerCase());
			if (child == null) {
				String ptrDownName = ptrDown.toString();
				throw new LookupException("Access to undefined name " + vp + (ptrDownName.isEmpty() ? "" : " in " + ptrDownName), this, _input, _ctx, partialFound);
			} else {
				ptrDown = child;
				partialFound = true;
			}
		}
		return ptrDown;
	}

	public boolean isImplicitVariable(List<Token> variablePart) {
		String vp = ((Token) variablePart.get(variablePart.size()-1)).getText().toLowerCase();
		switch (vp) {
			// ADODB.Recordset
			case "locktype":
			case "cursortype":
			case "cursorlocation":
			case "sort":
			case "fields":
			case "append": // method
			case "movenext": // method
			case "movefirst": // method
			case "movelast": // method
			case "addnew": // method
			case "update":
			// RegExp
			case "pattern":
			case "global":
			case "ignorecase":
			// Scripting.Dictionary
			case "item":
			case "add": // method
			// ADODB.Connection
			case "connectiontimeout":
			case "commandtimeout":
			// ADODB.Stream
			case "type":
			case "position":
			case "charset":
			case "open": // method
			case "close": // method
			case "writetext":
			case "write":
				return true;
			default:
				if (getImplicitVariables().contains(vp)) {
					return true;
				}
				return false;
		}
	}

}

AND: 'and';
ASP_END: '%>';
ASP_START: '<%';
BYREF: 'byref';
BYVAL: 'byval';
CALL: 'call';
CASE: 'case';
CLASS: 'class';
CONST: 'const';
DIM: 'dim';
DO: 'do';
EACH: 'each';
ELSE: 'else';
ELSEIF: 'elseif';
END: 'end';
ERROR: 'error';
EXIT: 'exit';
FOR: 'for';
FUNCTION: 'function';
GOTO: 'goto';
IF: 'if';
IN: 'in';
MOD: 'mod';
NEXT: 'next';
LOOP: 'loop';
ON: 'on';
OR: 'or';
REDIM: 'redim';
RESUME: 'resume';
PRESERVE: 'preserve';
PRIVATE: 'private';
PUBLIC: 'public';
SELECT: 'select';
SET: 'set';
STEP: 'step';
SUB: 'sub';
THEN: 'then';
TO: 'to';
UNTIL: 'until';
WEND: 'wend';
WITH: 'with';
WHILE: 'while';
XOR: 'xor';

parse:
	script EOF
;

script:
	(
		(~( ASP_START ) )*
		ASP_START NEWLINES*
		(
			( atom NEWLINES* )*
		) ASP_END
	)*
	NEWLINES*
;

atom:
	declaration | statement
;

// ----------------------------------------------------------------------------
// declarations
// ----------------------------------------------------------------------------

declaration:
	constDeclaration | dimDeclaration | functionDeclaration | subDeclaration | classDeclaration
;

constDeclaration:
	( PRIVATE | PUBLIC )? CONST ID '=' referenceExpression
	{
		checkDuplicate(new VariableElement($ID.text, ElementType.CONSTANT, current, new DeclarationException("LINE DETERMINATION", this, _input, _ctx)));
	}
;

dimDeclaration:
	( dimType=PUBLIC | dimType=PRIVATE | dimType=DIM | ( dimType=REDIM PRESERVE? ) ) variableNames += ID ( '(' ( expression ( ',' expression )* )? ')' )? ( ( '&' '_' NEWLINES )? ',' ( '&' '_' NEWLINES )? variableNames += ID ( '(' ( expression ( ',' expression )* )? ')' )? )*
	{
		if ($variableNames != null) {
			for (Object variableNameToken : $variableNames) {
				String variableName = ((Token) variableNameToken).getText();
				if (REDIM == $dimType.type) {
					if (current.getChild(variableName.toLowerCase()) == null) {
						_errHandler.reportError(this, new DeclarationException("Variable " + variableName + " not defined yet", this, _input, _ctx));
					}
				} else {
					CallStackElement variable = new VariableElement(variableName, ElementType.VARIABLE, current, new DeclarationException("LINE DETERMINATION", this, _input, _ctx));
					checkDuplicate(variable);
				}
			}
		}
	}
;

modifier :
	PUBLIC | PRIVATE
;

functionDeclaration:
	modifier? FUNCTION functionName=ID ( '(' ( ( BYVAL | BYREF )? parameterNames += ID ( '(' ')' )? ( ',' ( BYVAL | BYREF )? parameterNames += ID ( '(' ')' )? )* )* ')' )? NEWLINES
	{
		CallStackElement child = new FunctionElement($functionName.text, ElementType.FUNCTION, current, new DeclarationException("LINE DETERMINATION", this, _input, _ctx));
		checkDuplicate(child);
		current = child;
		if ($parameterNames != null) {
			for (Object parameterToken : $parameterNames) {
				String parameterName = ((Token) parameterToken).getText();
				CallStackElement variable = new VariableElement(parameterName, ElementType.PARAMETER, current, new DeclarationException("LINE DETERMINATION", this, _input, _ctx));
				checkDuplicate(variable);
			}
		}
	}
	mixedBody
	END FUNCTION
	{ current = current.getParent(); }
;

subDeclaration:
	modifier? SUB subName=ID ( '(' ( ( BYVAL | BYREF )? parameterNames += ID ( '(' ')' )? ( ',' ( BYVAL | BYREF )? parameterNames += ID ( '(' ')' )? )* )* ')' )? /* FIXME: this break "sub xxx() %>": NEWLINES */
	{
		CallStackElement child = new FunctionElement($subName.text, ElementType.SUB, current, new DeclarationException("LINE DETERMINATION", this, _input, _ctx));
		checkDuplicate(child);
		current = child;
		if ($parameterNames != null) {
			for (Object parameterToken : $parameterNames) {
				String parameterName = ((Token) parameterToken).getText();
				CallStackElement variable = new VariableElement(parameterName, ElementType.PARAMETER, current, new DeclarationException("LINE DETERMINATION", this, _input, _ctx));
				checkDuplicate(variable);
			}
		}
	}
	mixedBody
	END SUB
	{ current = current.getParent(); }
;

classDeclaration:
	CLASS ID NEWLINES
	{
		CallStackElement child = new ClassElement($ID.text, current, new DeclarationException("LINE DETERMINATION", this, _input, _ctx));
		checkDuplicate(child);
		current = child;
	}
	classBody
	END CLASS
	{ current = current.getParent(); }
;

classBody:
	( ( dimDeclaration | functionDeclaration | subDeclaration )? NEWLINES )*
;

// ----------------------------------------------------------------------------
// statements
// ----------------------------------------------------------------------------

statement:
	setAssignmentStatement |
	subCallStatement |
	functionCallStatement |
	outputStatement |
	ifStatement |
	switchStatement |
	forStatement |
	acceptingDoStatement |
	rejectingDoStatement |
	whileStatement |
	exitStatement |
	onErrorStatement |
	withStatement
;

setAssignmentStatement:
	SET? assignmentStatement
;

assignmentStatement:
	// "end" is a reserved keyword, but also a valid term here
	( variablePart+=ID ( arguments )? '.' )* variablePart+=( ID | END ) ( arguments )? '=' expression
	{
		LookupException downNotFound = null;
		CallStackElement ptrUp = current;
		CallStackElement child = null;
		do {
			downNotFound = null;
			try {
				child = isValidCallStack(ptrUp, $variablePart);
			} catch (LookupException e) {
				downNotFound = e;
			}
			ptrUp = ptrUp.getParent();
		} while (child == null && downNotFound != null && !downNotFound.isPartialFound() && ptrUp != null);
		if (downNotFound != null) {
			// TODO: Display as a warning because we don't validate variable types yet
			if (!isImplicitVariable($variablePart)) {
				_errHandler.reportError(this, downNotFound);
			}
		}

		if ($expression.cse != null) {
			if (child != null) {
				((VariableElement) child).setPrototype($expression.cse);
			}
		}
	}
;

functionCallStatement:
	CALL? functionCall
;

// Same as above, but without CALL
functionCall:
	// "end" is a reserved keyword, but also a valid term here
	( functionCallPart+=ID ( arguments )? '.' )* functionCallPart+=( ID | END ) arguments
	{
		LookupException downNotFound = null;
		CallStackElement ptrUp = current;
		CallStackElement child = null;
		do {
			downNotFound = null;
			try {
				child = isValidCallStack(ptrUp, $functionCallPart);
			} catch (LookupException e) {
				downNotFound = e;
			}
			ptrUp = ptrUp.getParent();
		} while (child == null && downNotFound != null && !downNotFound.isPartialFound() && ptrUp != null);
		if (downNotFound != null) {
			if (!isImplicitVariable($functionCallPart)) {
				_errHandler.reportError(this, downNotFound);
			}
		}
	}
;

subCallStatement:
	// "end" is a reserved keyword, but also a valid term here
	( subCallPart+=ID ( arguments )? '.' )* subCallPart+=( ID | END ) ( expressionList )?
	{
		LookupException downNotFound = null;
		CallStackElement ptrUp = current;
		CallStackElement child = null;
		do {
			downNotFound = null;
			try {
				child = isValidCallStack(ptrUp, $subCallPart);
			} catch (LookupException e) {
				downNotFound = e;
			}
			ptrUp = ptrUp.getParent();
		} while (child == null && downNotFound != null && !downNotFound.isPartialFound() && ptrUp != null);
		if (downNotFound != null) {
			if (!isImplicitVariable($subCallPart)) {
				_errHandler.reportError(this, downNotFound);
			}
		}
	}
;

outputStatement:
	'=' ( '(' expression ')' | expression )
;

ifStatement:
	IF expression THEN
	(
		( ( statement | expression ) ( ELSE ( statement | expression ) )? ( END IF )? )
		| ( /* FIXME: this break "if XXX then %>": NEWLINES */
			mixedBody
			( ELSEIF expression THEN /* FIXME: this break "else if XXX then %>": NEWLINES */
			mixedBody)*
			( ELSE /* FIXME: this break "else %>": NEWLINES */
			mixedBody)?
			END IF
		)
	)
;

switchStatement:
	SELECT CASE expression ':'? NEWLINES
	switchBlockLabels
	END SELECT
;

switchBlockLabels:
	switchCaseLabels
;

switchCaseLabels:
	switchCaseLabel*
;

switchCaseLabel:
	CASE ( ( postfixedExpression ( ',' postfixedExpression )* ) | ELSE ) ':'? NEWLINES?
	mixedBody
;

forStatement:
	FOR ( ( EACH ID IN expression ) | ( expression TO expression ( STEP expression )? ) ) /* FIXME: this break "for i = 1 to 10 %>": NEWLINES */
	mixedBody
	NEXT
;

acceptingDoStatement:
	DO NEWLINES
	mixedBody
	LOOP (( WHILE | UNTIL ) expression)?
;

rejectingDoStatement:
	DO ( WHILE | UNTIL ) expression NEWLINES
	mixedBody
	LOOP
;

whileStatement:
	WHILE expression NEWLINES
	mixedBody
	WEND
;

exitStatement:
	// FIXME: only allow FOR within for loops, FUNCTION within functions, SUB within subs, etc.
	EXIT ( FOR | DO | FUNCTION | SUB )
;

onErrorStatement:
	ON ERROR ( ( RESUME NEXT) | ( GOTO INTEGER ) )
;

withStatement:
	WITH withName=ID NEWLINES
	{
		CallStackElement child = new VariableElement($withName.text, ElementType.WITH, current, new DeclarationException("LINE DETERMINATION", this, _input, _ctx));
		// We can't use checkDuplicate here, as it would not make any sense
		current.addChild(child, this, _input, _ctx);
		current = child;
	}
	(
		( ( CALL? '.' functionCall ) | ( '.'? ( assignmentStatement | subCallStatement ) ) ) NEWLINES
	)*
	END WITH
	{ current = current.getParent(); }
;

// ----------------------------------------------------------------------------
// expressions
// ----------------------------------------------------------------------------

qualifiedIdentifier:
	ID ( '.' ID )*
;

literal:
	BOOL |
	INTEGER |
	FLOAT |
	STRING |
	'null' |
	'nothing'
;

primaryExpression:
	literal |
	parenthesizedExpression
;

postfixedExpression:
	( primaryExpression |
		// FIXME: only allow leading '.' if we are in a with statement
		'.'? ( ( ID ( arguments )? ( '(' expression ')' )? ) '.' )* ( ID ( arguments )? ( '(' expression ')' )? )
	)
;

negationExpression:
	'not'? postfixedExpression
;

unaryExpression:
	'-'? negationExpression
;

referenceExpression:
	'&'? unaryExpression
;

multiplicativeExpression:
	referenceExpression ( ( '*' | '/' | MOD | '\\' ) referenceExpression )*
;

additiveExpression:
	multiplicativeExpression ( ( '+' | '-' ) multiplicativeExpression )*
;

relationalExpression:
	additiveExpression ( ( '<' | '<=' | '>=' | '>' ) additiveExpression )*
;

equalityExpression:
	relationalExpression ( ( '=' | '<>' ) relationalExpression )*
;

exclusiveOrExpression:
	equalityExpression ( XOR equalityExpression )*
;

logicalAndExpression:
	exclusiveOrExpression ( AND exclusiveOrExpression )*
;

logicalOrExpression:
	logicalAndExpression ( OR logicalAndExpression )*
;

concatenation:
	( logicalOrExpression ( '&' ( '_' NEWLINES )? logicalOrExpression )* )
;

expression returns [CallStackElement cse]:
	( concatenation | instantiationExpression { $cse = $instantiationExpression.cse; } )
;

expressionList:
	expression ( ( '_' NEWLINES )? ','+ ( '_' NEWLINES )? expression )*
;

arguments:
	'(' expressionList? ')'
;

subArguments:
	expressionList?
;

parenthesizedExpression:
	'(' expression ')'
;

instantiationExpression returns [CallStackElement cse]:
	'new' className=ID
	{
		CallStackElement element = root.getChild($className.getText().toLowerCase());
		if (element == null) {
			_errHandler.reportError(this, new DeclarationException("No class definition found for instantiation " + $className.getText(), this, _input, _ctx));
		} else if (element.type != ElementType.CLASS) {
			_errHandler.reportError(this, new DeclarationException("Trying to instantiate " + element.type + " " + element.name + " which is not a " + ElementType.CLASS, this, _input, _ctx));
		}
		$cse = element;
	}
;

// ----------------------------------------------------------------------------
// ASP
// ----------------------------------------------------------------------------

mixedBody:
	(
		( dimDeclaration | constDeclaration | statement )?
		// Having a const declaration inside a method is valid, but I'm really unsure about it's lifetime
		NEWLINES |
		( ASP_END ( ~( ASP_START ) | ( '{' | '}' | '[' | ']' | ';' | '?' | '!' | '$' | '|' | '#' | '^' | '~' | 'ä' | 'ö' | 'ü' | 'ß' | '€' | '®' | '.' | ',') )* ASP_START )
	)*
;

// ----------------------------------------------------------------------------
// fragments
// ----------------------------------------------------------------------------

fragment DIGIT: '0'..'9';
fragment LETTER: 'a'..'z';
fragment NEWLINE: '\r' | '\n';

// ----------------------------------------------------------------------------
// Simple types
// ----------------------------------------------------------------------------
BOOL: 'true' | 'false';
INTEGER: DIGIT+;
FLOAT: DIGIT+ '.' DIGIT+;
STRING: '"' ( '"' '"' | ~( '"' | '\r' | '\n' ) )* '"';

ID: LETTER ( LETTER | DIGIT | '_' )*;

NEWLINES: ( NEWLINE )+ ;
WHITESPACE: (' ' | '\t') { setChannel(HIDDEN); } ;
COMMENT: '\'' ~ ( '\r' | '\n' )* { setChannel(HIDDEN); } ;
