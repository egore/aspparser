package de.egore911.aspparser.exceptions;

import org.antlr.v4.runtime.IntStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

public class DeclarationException extends RecognitionException {

	private static final long serialVersionUID = 3367425410116330027L;

	public DeclarationException(String message, Recognizer<?, ?> recognizer, IntStream input, ParserRuleContext ctx) {
		super(message, recognizer, input, ctx);
	}

}
