package de.egore911.aspparser.exceptions;

import org.antlr.v4.runtime.IntStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

public class LookupException extends RecognitionException {

	private static final long serialVersionUID = 2404859440374378183L;
	private final boolean partialFound;

	public LookupException(String message, Recognizer<?, ?> recognizer, IntStream input, ParserRuleContext ctx, boolean partialFound) {
		super(message, recognizer, input, ctx);
		this.partialFound = partialFound;
	}

	public boolean isPartialFound() {
		return partialFound;
	}

}
