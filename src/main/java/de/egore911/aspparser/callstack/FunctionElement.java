package de.egore911.aspparser.callstack;

import org.antlr.v4.runtime.RecognitionException;

public class FunctionElement extends CallStackElement {

	public FunctionElement(String name, ElementType type, CallStackElement parent, RecognitionException e) {
		super(name, type, parent, e);
	}

	public FunctionElement(String name, ElementType type, CallStackElement parent, String occurrence) {
		super(name, type, parent, occurrence);
	}

	@Override
	public boolean isAllowedChild(ElementType type) {
		switch (type) {
		case CONSTANT:
		case VARIABLE:
		case PARAMETER:
		case WITH:
			return true;
		case CLASS:
		case FUNCTION:
		case SUB:
		default:
			return false;
		}
	}

	@Override
	public CallStackElement isAllowedChild(String elementName) {
		CallStackElement child = super.isAllowedChild(elementName);
		if (child == null && elementName.equalsIgnoreCase(name)) {
			child = this;
		}
		return child;
	}
}
