package de.egore911.aspparser;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

class HelloWorldTest {

    @Test
    void testHello() throws IOException {
        InputStream resourceAsStream = this.getClass().getResourceAsStream("/hello.asp");
        Assertions.assertNotNull(resourceAsStream);
        CharStream charStream = CharStreams.fromStream(resourceAsStream);
        AspLexer lexer = new AspLexer(charStream);
        TokenStream tokenStream = new CommonTokenStream(lexer);
        AspParser parser = new AspParser(tokenStream);
        parser.init();
        var parse = parser.parse();
        Assertions.assertNotNull(parse);
    }

    @Test
    void testHtml1() throws IOException {
        InputStream resourceAsStream = this.getClass().getResourceAsStream("/html1.asp");
        Assertions.assertNotNull(resourceAsStream);
        CharStream charStream = CharStreams.fromStream(resourceAsStream);
        AspLexer lexer = new AspLexer(charStream);
        TokenStream tokenStream = new CommonTokenStream(lexer);
        AspParser parser = new AspParser(tokenStream);
        parser.init();
        var parse = parser.parse();
        Assertions.assertNotNull(parse);
    }

    @Test
    void testHtml2() throws IOException {
        InputStream resourceAsStream = this.getClass().getResourceAsStream("/html2.asp");
        Assertions.assertNotNull(resourceAsStream);
        CharStream charStream = CharStreams.fromStream(resourceAsStream);
        AspLexer lexer = new AspLexer(charStream);
        TokenStream tokenStream = new CommonTokenStream(lexer);
        AspParser parser = new AspParser(tokenStream);
        parser.init();
        var parse = parser.parse();
        Assertions.assertNotNull(parse);
    }

}
