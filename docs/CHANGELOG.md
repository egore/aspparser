## [1.0.3](https://gitlab.com/appframework/egore-personal/aspparser/compare/1.0.2...1.0.3) (2025-02-26)


### Bug Fixes

* **deps:** update dependency org.slf4j:slf4j-api to v2.0.17 ([cf5f90b](https://gitlab.com/appframework/egore-personal/aspparser/commit/cf5f90b14a1a9d90382d868025c2656544959c25))

## [1.0.2](https://gitlab.com/appframework/egore-personal/aspparser/compare/1.0.1...1.0.2) (2024-12-18)

## [1.0.1](https://gitlab.com/appframework/egore-personal/aspparser/compare/v1.0.0...1.0.1) (2024-10-22)

# 1.0.0 (2024-10-09)


### Bug Fixes

* **deps:** update dependency info.picocli:picocli to v4.7.6 ([2043436](https://gitlab.com/appframework/egore-personal/aspparser/commit/20434365f7fd3ef2df6583562db5e7e9a2e9dab2))
* **deps:** update dependency org.slf4j:slf4j-api to v2.0.13 ([2a4d11f](https://gitlab.com/appframework/egore-personal/aspparser/commit/2a4d11f9c78122d5484e619d72522722367da4b3))
* **deps:** update dependency org.slf4j:slf4j-api to v2.0.16 ([e9c6eeb](https://gitlab.com/appframework/egore-personal/aspparser/commit/e9c6eebdb4ff34b1a49c9a384c7fc6af942e815e))


### Features

* **antlr:** Upgrade from antlr 3 to 4 ([bf475b9](https://gitlab.com/appframework/egore-personal/aspparser/commit/bf475b993919df2e322ed5a329482f5470ba79d9))
* **cli:** Switch to picocli ([7f345ca](https://gitlab.com/appframework/egore-personal/aspparser/commit/7f345ca932d90b9313feb0cd51ff58f2d75f0e48))
* **cli:** Use slf4j ([e7bdaba](https://gitlab.com/appframework/egore-personal/aspparser/commit/e7bdaba948bffd85888a9e544ff78b8e35af5aa4))
