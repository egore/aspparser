aspparser - A ANTLR based parser for ASP source code
====================================================

Usage
-----

The main class of the ASP parser is "de.egore911.aspparser.CheckDirectory". This
class expects the name of directory as its first command line argument. The
parser will then work with all "\*.asp" files that are found within this
directory or its subdirectories.

Features
--------

The parser currently supports very basic ASP syntax, including:

- Branching statements: if, elseif, else, case
- Looping statements: for, do, while
- Structural statements: function, sub, class

It will validate the syntax of plain ASP code that is _not_ mixed with HTML
(this is the feature currently being worked on).

### Name collisions

The parser is capable to detect methods or variables having the same name. The
detection will add errors to the parser for each occourence:

    line 123:1 function myDuplicateFuntion already defined

Partial implemented
-------------------

### Strict validation of ASP

Due to the interpreted nature of ASP the IIS-Server will only validate code
paths that are actually used when rendering a page. This has a major downside,
because it is hard to test every branch within the code. The final aim of this
parser is to validate the complete code so chances are minimal it will fail due
to syntactic errors.

The parser will work similar to "Option Explicit", so you are aware of
accessing undeclared variables or functions. This will be an optional feature
of the parser, eventhough enabled by default.


Missing functionality
---------------------

### Warning about accessing global variables in methods

Global variables are kind of risky when used without caution. You can, for
example, accidentially change the value of a global variable without noticing
it. On the other hand your code can fail after refactoring, because a global
variable was removed. The parser will be able to detect usage of global
variables and generate a warning for this. This will be an optional feature of
the parser, eventhough enabled by default.

### Unnecessary code detection

When running an interpreted language on a server it can cause overhead due to
unnecessary evaluation of code that is not relevant for rendering. This happens
especially when using lots of includes. The parser will evaluate the complete
code and list all methods and variables that are only declared/assigned and
never accessed/called.

### Wrong variable assignments

ASP requires 'set' in assignment statements for complex objects. This can
easily be forgotten so the parser should have a check to inform the developer
about this.

    dim a, b
    set a = new MyClass
    b = a

The third line is actually invalid. The parser will remember what type a
variable contains and warn about incompatible types. This might also be used
for other incompatible assignments, e.g.

    dim a
    a = true
    if a = "1" then
     a = false
    end if

This example demonstrates that a string is compared to a boolean, which might
not yield the expected result.

### Events

Events are not recognized yet. Support will be added when needed.
